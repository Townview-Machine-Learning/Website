 # This is the Code for the Club Website

 Please make changes to improve our website. We are very limited in our knowledge of web development, so please help us.

 There are only two rules that must always be followed with your changes:
1. No JavaScript - We don't want a bloated website, it should even be possible to view the website in a terminal
2. It must have a dark theme - Don't blind us

[Our Club Website](https://www.townviewai.com/) | [Our Cloud Computing Service](https://shell.townviewai.com/) |  [Our Discord Server](https://discord.gg/MTChRGn) | [Our YouTube Channel](https://www.youtube.com/channel/UCtoopcfdAcxFliCMCVt93qA) | [Our Twitch Channel](https://www.twitch.tv/townviewml) | [Our Instagram](https://www.instagram.com/townviewml/)
